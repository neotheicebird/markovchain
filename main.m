%% This script does a experiment to find the markov chain of a set set of state transitions

clc
clear all
addpath(genpath(fileparts(mfilename('fullpath'))));

% user parameters
datfilename = 'E:\Markov Chain - Ahmed\data\Sep29-occupdata.txt';
thresVec = [0, 5, 8]; % vector of 3 elements which helps threshold and classify 
                      % the data into states [E,F,M,C]. here <= 0 is E,
                      % else if <= 5 is F, else if <= 8 is M, else C.

[traindata, colnames] = LoadData(datfilename); % we can have different training and real data
states = StatesFromData(traindata, thresVec);
transitionmatrix = LearnTransition(states);

% We find the probabilities of transition from one state to another using
% the `realdata` and `transitionmatrix`

% realdata = traindata; % in this case
probabilitymatrix = FetchProbabilityMatrix(states, transitionmatrix);